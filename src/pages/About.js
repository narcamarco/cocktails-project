import React from 'react';

const About = () => {
  return (
    <section className="section about-section">
      <h1 className="section-title">about us</h1>
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit commodi
        temporibus, quam vitae magni ipsam molestias? Hic dolore iusto officiis
        quod rem delectus accusantium? Officia quidem inventore voluptatibus est
        dignissimos, consectetur perspiciatis voluptate numquam fuga excepturi,
        ducimus, ratione ea hic.
      </p>
    </section>
  );
};

export default About;
